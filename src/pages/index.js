import React from "react"
import { Link } from "gatsby"

import Feature from "../components/Feature"


import { UserShield } from "styled-icons/fa-solid/UserShield"
import { CreditCard } from "styled-icons/fa-solid/CreditCard"
import { CreativeCommons } from "styled-icons/boxicons-logos/CreativeCommons"
import { Eye } from "styled-icons/fa-regular/Eye";
import { UserVoice } from "styled-icons/boxicons-solid/UserVoice";
import { PiggyBank } from "styled-icons/fa-solid/PiggyBank";

import Layout from "../components/layout"
import SEO from "../components/seo"
import Card from "../components/Card"
import Section from "../components/Section"


import staticdata from "../../staticdata.json";
import Cell from "../components/Cell"
import styled from "styled-components"
const IndexPage = () => {return (
  <Layout>
    <SEO title="Home" />
    <div className="Hero">
      <div className="HeroGroup">
        <h1>Virtual Democracies</h1>
        <p>Usuários Associados Livres</p>
        <Link to="/page-2/">Download</Link>
        <div className="Logos">
          <PhoneLock size={36} />
          <PaymentsIcon size={36} />
          <OpenSoftwareIcon size={36} />
          <AuditIcon size={36} />
          <VoteIcon size={36} />
          <PiggyRecyclerFundIcon size={36} />
          
        </div>
       

      </div>
      </div>
      <Feature title="Dados Proprietários" text="Segurança de que nenhum dado seu está sendo usado por nínguem que não seja você mesmo.">
        <PhoneLock size={160} />
      </Feature>

      <Feature title="Proxy de Pagamento Seguro" text="O método de pagamento protege a liberdade dos usuários, permitindo que com um cartão de crédito - ou débito - você realize pagamentos sem que nenhum dos agentes intermediários possa vizualizar a transação até o ponto final. Até os grandes bancos e operadores passam a obter apenas informação fragmentada de suas escolhas.">
      <PaymentsIcon size={160} />
      </Feature>

      <Feature title="Código Aberto" text="O código da plataforma é completamente aberto para que a comunidade geral comprove a sinceridade e qualidade do código em atender as proposições da Vidas. Além de permitir que a comunidade possa trabalhar em conjunto para melhorar o código.">
        <OpenSoftwareIcon size={160} />
      </Feature>

      <Feature title="Open Ops" text="Não só o código é aberto, mas cadeia de decisões e o log das operações também estarão disponíveis. Atendendo aos padrões de transparência brutal.">

      <AuditIcon size={160} />
      </Feature>

      <Feature title="Comando de Baixo" text="Aplicamos o modelo de governança de baixo para cima.  Em que os usuários efetivamente, possuem os meios de garantir que a operação atenda os seus interesses. Blindando todos de situações em que os interesses dos gestores possam resultar em decisões prejudiciais aos usuários.">

      <VoteIcon size={160} />
      </Feature>

      <Feature title="Piggy Fund" text="Sendo a Vidas uma associação sem fins lucrativos, mas geradora de receita. Todo e qualquer valor operacional remanescente, não comprometido com nenhuma obrigação determinada pelo descritivo operacional da plataforma, será revertido em valor disponível para projetos e iniciativas participativas.">

      <PiggyRecyclerFundIcon size={160} />
          
      </Feature>
      
      <div className="Cards">
        <h2>Plataformas Signatárias</h2>
        <div className="CardGroup">   
          <Card title="Olkeey" text="Event Agency" image={require("../images/olkeey_white.svg")} />
          <Card title="Microappollis" text="City of MicroApps" image={require("../images/mapps.svg")} />
          
        </div>  
      </div>
      <Section
        background={"linear-gradient(104deg, rgba(0, 189, 211) 0%, rgb(28, 130, 200) 100%)"}
        image={require("../images/wallpaper2.jpg")}
        logo={require("../images/mapps_white.svg")}
        title="Microappollis"
        text="A all in one App, for you to purchase all the services, from a massage, a guitar lesson, to a mango at the street, or a construction service for your house."
      />
      <SectionCaption>Available apps</SectionCaption>
        <SectionCellGroup>{staticdata.cells.map(cell => (
          <Cell title={cell.title} image={cell.image} />
       ))}
        </SectionCellGroup>

        <Section
        background={"linear-gradient(104deg, #ff153f 0%, #ff7900 100%)"}
        image={require("../images/wallpaper2.jpg")}
        logo={require("../images/olkeey_white.svg")}
        title="OLKEEY"
        text="Artists and Everything you need to build increadible events."
      />
      <SectionCaption>Available apps</SectionCaption>
        <SectionCellGroup>{staticdata.cells_olkeey.map(cell => (
          <Cell title={cell.title} image={cell.image} />
       ))}
        </SectionCellGroup>
    
  </Layout>
)
        }
export default IndexPage;

const SectionCaption = styled.p`
  font-weight: 600;
  font-size: 24px;
  text-transform: uppercase;
  color: #222;
  text-shadow: 1px 1px 0 #fff;
  text-align: flex-start;
  padding: 0 0 0 36px;
`
const SectionCellGroup = styled.div`
  max-width: 800px;
  margin: 0 auto 100px;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 12px;
  padding: 28px;
  
  @media (max-width: 740px) {
    grid-template-columns: repeat(1, 1fr);
  }
`

const PhoneLock = styled(UserShield)`
  color: rgba(49,56,60);;
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);

  :hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`


const PaymentsIcon = styled(CreditCard)`
  color: rgba(49,56,60);
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);

  :hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`

const OpenSoftwareIcon = styled(CreativeCommons)`
  color: rgba(49,56,60);
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);

  :hover {
    transform: scale(1.2);
    cursor: pointer;
  }

  
`

const AuditIcon = styled(Eye)`
  color: rgba(49,56,60);
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);

  :hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`


const VoteIcon = styled(UserVoice)`
  color: rgba(49,56,60);
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);

  :hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`

const PiggyRecyclerFundIcon = styled(PiggyBank)`
  color: rgba(49,56,60);
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);

  :hover {
    transform: scale(1.2);
    cursor: pointer;
  }
`