import { Link } from "gatsby"
import PropTypes from "prop-types"

import logo from "../images/logo-vidas.svg"
import "./styles/header.css"
import styled from "styled-components"

import React, { Component } from 'react';

class Header extends Component {

  state = {
    hasScrolled: false
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll)
  }


  handleScroll = (e) => {
    const scrollTop = window.pageYOffset

    if (scrollTop > 50) {
      this.setState({ hasScrolled: true})
    }
    else {
      this.setState({ hasScrolled: false })
    }
  }

  render() {
    
    return (
      <div className={this.state.hasScrolled ? "Header HeaderScrolled" : "Header"}>
       <div className="HeaderGroup">
       <LogoContainer>
      <Link to="/"><img src={logo} width="20"/></Link>
       </LogoContainer>
      <Link to="courses">Documentos</Link>
      <Link to="workshops">Tecnologia</Link>
      <Link to="downloads">Auditoria aberta</Link>
      <Link to="/buy"><button>Fundo Aberto</button></Link>
  
    </div>
  </div>
    );
  }
}

export default Header;
// #dbc312
const LogoContainer = styled.div`
  
  padding: 8px 20px;
  border-radius: 20px 2px 2px 10px;
  background-color: #EBECF0;
  box-shadow: -5px -5px 20px #fff,  5px 5px 20px #BABECC;
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
  :hover {
    box-shadow: -2px -2px 5px #fff, 2px 2px 5px #BABECC;
  }

  :active {
    box-shadow: inset 1px 1px 2px #BABECC, inset -1px -1px 2px #fff;
  }
`