import React from 'react';
import styled from 'styled-components';



const Feature = (props, { children }) => {
    return (
        <SectionGroup image={props.image}>
                <SectionImage>{props.children}</SectionImage>
                <SectionTitleGroup>
                <SectionTitle>{props.title}</SectionTitle>
                <SectionText>{props.text}</SectionText>
            </SectionTitleGroup>
            <div style={{width: "100vw", display:"flex", justifyContent: "center", alignItems: "center"}}>
                <div style={{width: "80px", border: "1px solid rgba(0,0,0,0.08)"}}></div>
            </div>
        </SectionGroup>
    )
}

export default Feature;

const SectionGroup = styled.div`
    background: white;
    height: 100vh;
    
    display: grid;
    grid-template-rows: repeat(3,1fr);
    
    align-items: center;
    justify-content: center;
    position: relative;
`
const SectionImage = styled.div`
    display: flex;
    justify-content: center;
    align-items: flex-end;
    align-self: flex-end;
    margin-bottom: -18px;
    
    
`


const SectionTitleGroup = styled.div`
    display: grid;
    grid-template-rows: 1fr;
    margin: 10px 80px;
    
    
    
    position: relative;
    z-index: 2;

    @media (max-width: 720px) {
        grid-template-columns: 1fr;
    }
`


const SectionTitle = styled.h3`
    color: rgb(40,56,60);
    font-size: 40px;
    margin: auto 0;
    line-height: auto;
    position: relative;
    z-index: 2;
    text-align: center;

    @media (max-width: 640px) {
        font-size: 32px;
    }
`


const SectionText = styled.p`
    color: rgba(0,0,0,0.8);
    position: relative;
    z-index: 2;
    text-align: center;
    line-height: 40px;
`


